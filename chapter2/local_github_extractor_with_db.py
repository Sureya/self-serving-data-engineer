# in-built
import multiprocessing
import logging


# 3rd party
import requests
from ratelimit import limits


@limits(calls=5000, period=60 * 60)
def scrape_api(url):
    print(f"Scrapping {url}")
    api_response = requests.get(url)
    return api_response.json()


class Repository:
    def __init__(self, details):
        self.data = details
        self.validation_result = self._validate()

        if self.validation_result:
            self.id = self.data["id"]
            self.name = self.data["name"]
            self.description = self.data["description"]
            self.total_downloads = self.get_download_details()
            self.owner_id, self.owner_login, self.owner_profile, self.owner_type = self.get_owner_info()
            self.pprint()
        else:
            raise Exception("Invalid JSON structure for Repository")

    def _validate(self):
        required_keys = ["id", "name", "description", "url", "downloads_url", "owner"]
        return all([x in self.data.keys() for x in required_keys])

    def get_download_details(self):
        download_response = scrape_api(self.data["downloads_url"])
        return len(download_response)

    def get_owner_info(self):
        owner = self.data["owner"]
        return owner["id"], owner["login"], owner["url"], owner["type"]

    def pprint(self):
        print(self.id, "fetched")

    def add_users(self):
        user = User(self.data["owner"])
        following_users = user.get_followers_details(minimal=False)

        for this_user in following_users:
            User(this_user)


class User:
    def __init__(self, details):
        self.data = details
        self.validation_result = self._validate()

        if self.validation_result:
            self.id = self.data["id"]
            self.login = self.data["login"]
            self.total_followers = self.get_followers_details()
            self.total_subscriptions = self.get_subscription_details()
            self.total_organisations = self.get_organisation_details()
            self.pprint()
        else:
            raise Exception("Invalid JSON structure for User")

    def _validate(self):
        if isinstance(self.data, dict):
            required_keys = ["id", "login", "followers_url", "subscriptions_url", "organizations_url"]
            return all([x in self.data.keys() for x in required_keys])
        else:
            print(self.data)
            return False

    def get_followers_details(self, minimal=True):
        followers_response = scrape_api(self.data["followers_url"])
        if minimal:
            return len(followers_response)
        else:
            return followers_response

    def get_subscription_details(self):
        subscription_response = scrape_api(self.data["subscriptions_url"])
        return len(subscription_response)

    def get_organisation_details(self):
        organisation_response = scrape_api(self.data["organizations_url"])
        return len(organisation_response)

    def pprint(self):
        print("\t", self.id, "gathered")


def worker(param):
    repo = Repository(param)
    repo.add_users()


if __name__ == '__main__':
    repo_url = "https://api.github.com/repositories?access_token=578347e6edeea47b2b0abb323b52076d5048b06b"
    response = scrape_api(repo_url)

    p = multiprocessing.Pool(15)
    map_return = p.map(worker, response)
    p.close()
    p.join()
