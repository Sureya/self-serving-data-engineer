create table users(
        id INT
    ,   login TEXT
    ,   total_followers INT
    ,   total_subscriptions INT
    ,   total_organisations INT
    ,   PRIMARY KEY (id)

);

create table repository(
      id INT
    , name TEXT
    , description TEXT
    , url TEXT
    , total_downloads INT
    , owner_id TEXT
    , owner_login TEXT
    , owner_profile TEXT
    , owner_type TEXT
    , PRIMARY KEY (id)
);


create table repo_branches(
      repo_id INT REFERENCES repository(id)
    , branch_name text
    , PRIMARY KEY (repo_id, branch_name)
);


create table repo_languages(
      repo_id INT REFERENCES repository(id)
    , language TEXT
    , lines INT
    , PRIMARY KEY (repo_id, language)
);

create table repo_contributors(
        repo_id INT REFERENCES repository(id)
    ,   contributor_id INT REFERENCES users(id)
    ,   lines_contributed INT
    ,   PRIMARY KEY (repo_id, contributor_id)
);

